// server/index.js
import express from "express";
import data from "../dbusers.js";
import * as crypto from "crypto";
const PORT = process.env.PORT || 3002;

const app = express();
app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});

app.get("/api", (req, res) => {
  res.json({ message: "Hello from server!" });
});

app.get("/api/getTotalCount", (req, res) => {
  res.json({ data: data.results.length });
});

app.get("/api/genderStat", (req, res) => {
  const female = data.results.filter((obj) => obj.gender === "female").length;
  const male = data.results.filter((obj) => obj.gender === "male").length;

  res.json({ female: (100 * female) / 5000, male: (100 * male) / 5000 });
});

app.get("/api/searchOne", async (req, res) => {
  let response = data.results;

  if (req.query.firstname && req.query.firstname.length > 0) {
    response = response.filter(
      (obj) => req.query.firstname && req.query.firstname === obj.name.first
    );
  }
  if (req.query.lastname && req.query.lastname.length > 0) {
    response = response.filter(
      (obj) => req.query.lastname && req.query.lastname === obj.name.last
    );
  }
  if (req.query.gender && req.query.gender.length > 0) {
    response = response.filter(
      (obj) => req.query.gender && req.query.gender === obj.gender
    );
  }
  if (req.query.state && req.query.state.length > 0) {
    response = response.filter(
      (obj) => req.query.state && req.query.state === obj.location.state
    );
  }
  if (req.query.city && req.query.city.length > 0) {
    response = response.filter(
      (obj) => req.query.city && req.query.city === obj.location.city
    );
  }
  if (req.query.phone && req.query.phone.length > 0) {
    response = response.filter(
      (obj) => req.query.phone && req.query.phone === obj.phone
    );
  }
  res.json({ response: response });
});

app.get("/user/:username", (req, res) => {
  var result = data.results.filter(
    (obj) => obj.login.username === req.params.username
  );

  res.json({ result });
});

app.get("/api/login", (req, res) => {
  const username = req.query.username;
  const password = req.query.password;
  if (!username || !password) {
    return res.json({ message: "Internal Error" });
  }

  const user = data.results.filter((obj) => obj.login.username === username);

  if (!user[0]) {
    return res.json({ message: "No account link to this username" });
  }
  const salt = user[0].login.salt;
  const key = password + salt;
  const crypt = crypto.createHash("sha256").update(key).digest("hex");

  if (user[0].login.sha256 !== crypt) {
    return res.json({ message: "Wrong password" });
  }

  res.json({ user: user });
});

app.get("/api/countryStat", (req, res) => {
  var country = [...new Set(data.results.map((item) => item.location.country))];
  var numberForEachCountry = [];
  var result = [];
  for (let index = 0; index < country.length; index++) {
    const element = country[index];

    numberForEachCountry.push(
      data.results.filter((obj) => obj.location.country === element).length
    );

    result = numberForEachCountry.map((e) =>
      parseFloat((e / 5000) * 100).toFixed(2)
    );
  }

  var parsedResult = {};

  for (let i = 0; i < country.length; i++) {
    parsedResult[country[i]] = result[i];
  }

  res.json({ data: parsedResult });
});
