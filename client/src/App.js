import React, { useState, useEffect } from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { Login } from "./Component/login/login";
import Search from "./Component/search/search";
import User from "./Component/user/user";
import { makeStyles } from "@mui/styles";
import Cookies from "universal-cookie";
import { Dashboard } from "./Component/myaccount/dashboard";
import { QueryClient, QueryClientProvider, useQuery } from "react-query";

const queryClient = new QueryClient();

const useStyles = makeStyles({
  title: {
    color: "#fff",
    textTransform: "uppercase",
    fontFamily: "Righteous",
    fontSize: "55px",
    padding: "13px 20px",
    margin: "1% !important",
    display: "block",
    width: "auto",
    border: "10px #ececec solid",
    borderRadius: "25px",
    textAlign: "center",
    maxWidth: "430px",
    float: "left"
  },
  fourHundredFour: {
    fontFamily: "Righteous",
    fontSize: "55px",
    textAlign: "center",
    width: "70%",
    margin: "10% auto",
  },
  btn: {
    textDecoration: "none",
    fontSize: "30px !important",
    fontFamily: "Righteous !important",
    padding: "15px !important",
    width: "66%",
    margin: "50px 18% !important",
    borderRadius: "20px",
    display: "block",
    textAlign: "center",
    background: "rgba(0,35,255,1)",
    color: "#fff !important",
    fontWeight: "bold !important",
    transition: "0.4s",
    "&:hover": {
      background: "radial-gradient(circle, rgba(75,100,255,1) 0%, rgba(0,35,255,1) 100%);",
    },
  },
});

const App = () => {
  const classes = useStyles();
  const cookies = new Cookies();
  const [cookie, setCookie] = useState();
  var isLoged = cookies.get("loged");
  useEffect(() => {
    setCookie(cookies.get("loged"));
  }, [cookie]);

  return (
    <div className="wrapper">
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <h1 className={classes.title}>People Stats</h1>
          <Routes>
            {cookie ? (
              <>
                <Route path="search" element={<Search />}></Route>
                <Route path="user/:username" element={<User />}></Route>
                <Route path="login" element={<Login />}></Route>
                <Route
                  path="dashboard/:username"
                  element={<Dashboard />}
                ></Route>
                <Route
                  path="*"
                  element={
                    <p className={classes.fourHundredFour}>
                      Erreur 404 ! La page que vous chercher n'existe pas, plus ou pas encore !
                      <a href={`/dashboard/${cookie}`} className={classes.btn}>
                        {" "}
                        Retour à l'accueil
                      </a>
                    </p>
                  }
                ></Route>
              </>
            ) : (
              <>
                <Route
                  path="*"
                  element={
                    <p>
                      Vous devez vous connecter<a href="/login"> ICI</a>
                      <br></br>
                      Si vous vous etes connecter rafraichissez la page
                    </p>
                  }
                ></Route>
                <Route path="login" element={<Login />}></Route>
              </>
            )}
          </Routes>
        </BrowserRouter>
      </QueryClientProvider>
    </div>
  );
};

export default App;
