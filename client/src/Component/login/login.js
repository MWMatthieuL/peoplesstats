import { TextField, Button } from "@mui/material";
import { useState } from "react";
import { useQuery } from "react-query";
import { useNavigate } from "react-router-dom";
import Cookies from "universal-cookie";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  error: {
    fontFamily: "Righteous !important",
    color: "#fff",
    margin: "0 18% 60px 18% !important",
    textAlign: "center"
  },
  title: {
    color: "#333",
    textAlign: "center",
    textTransform: "uppercase",
    fontFamily: "Righteous !important",
    fontSize: "50px",
    margin: "0 0 50px 0 !important",
  },
  loginInput: {
    width: "66%",
    margin: "0 18% 20px 18% !important",
  },
  loginBtn: {
    fontSize: "30px !important",
    fontFamily: "Righteous !important",
    padding: "15px !important",
    width: "66%",
    margin: "50px 18% !important",
    borderRadius: "20px",
    display: "block",
    textAlign: "center",
    background: "rgba(0,35,255,1)",
    color: "#fff !important",
    fontWeight: "bold !important",
    transition: "0.4s",
    "&:hover": {
      background: "radial-gradient(circle, rgba(75,100,255,1) 0%, rgba(0,35,255,1) 100%);",
    },
  },
  loginCard: {
    width: "58%",
    margin: "30px auto !important",
    padding: "5% 11%",
    borderRadius: "15px",
  },
});

export const Login = () => {
  const classes = useStyles();
  let navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState();
  const { data, refetch } = useQuery(
    "repoData",
    () =>
      fetch(`api/login/?username=${username}&password=${password}`)
        .then((res) => res.json())
        .then((data) => {
          if (!data.message) {
            const cookies = new Cookies();
            cookies.set("loged", username, { path: "/" });
            navigate(`/dashboard/${username}`);
          } else {
            setErrorMessage(data.message);
          }
        }),
    { enabled: false }
  );

  const HandleLogin = () => {
    refetch();
  };

  return (
    <>
      <div className={classes.loginCard}>
        {errorMessage && <p className={classes.error}>{errorMessage}</p>}
        <TextField
          className={classes.loginInput}
          label="username"
          onChange={(e) => setUsername(e.target.value)}
          value={username}
          color="primary"
          variant="standard"
        />
        <TextField
          className={classes.loginInput}
          label="password"
          onChange={(e) => setPassword(e.target.value)}
          color="primary"
          variant="standard"
          type="password"
        />
        <Button onClick={HandleLogin} className={classes.loginBtn}>Connexion</Button>
      </div>
    </>
  );
};
