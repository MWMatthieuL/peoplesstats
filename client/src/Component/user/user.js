import { useParams } from "react-router-dom";
import { useState } from "react";
import Typography from "@mui/material/Typography";
import { useQuery } from "react-query";
import {makeStyles} from "@mui/styles";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";

const useStyles = makeStyles({
  user: {
    fontFamily: "Righteous !important",
    margin: "30px 5% !important",

  },
  searchCard: {
    borderRadius: "15px !important",
    width: "70%",
    margin: "30px 15% !important",
    display: "inline-block",
    boxShadow: "0 12px 40px -12.125px rgba(0,0,0,0.3) !important",
  },
  searchCardContent: {
    display: "flex",
    alignItems: "center",
    flexWrap: "wrap",
  },
  img: {
    borderRadius: "5px !important",
    height: "300px",
    margin: "3%",
  },
});

const User = () => {
  const classes = useStyles();
  let { username } = useParams();
  const [user, setUser] = useState();
  const { isLoading, error } = useQuery("repoData", () =>
    fetch(`${username}`)
      .then((res) => res.json())
      .then((data) => setUser(data.result[0]))
  );
  if (isLoading) return "Loading...";

  if (error) return "An error has occurred: " + error.message;

  return (
    <>
      {user && (
        <>
          <Card sx={{ minWidth: 275 }} className={classes.searchCard}>
            <CardContent className={classes.searchCardContent}>
              <img src={user.picture.large}  className={classes.img}></img>
              <div className={classes.user}>
                <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                </Typography>
                <Typography sx={{ mb: 1.5 }} variant="h5" component="div">
                  {user.name.title} {user.name.first} {user.name.last}
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                  Inscrit il y a {user.registered.age} an(s) ({user.registered.date.slice(0, 4)})
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                  {user.gender}, {user.dob.age} ans (Né le {user.dob.date.slice(0, 10)})
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                  @ : {user.email}
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                  Tél. domicile : {user.phone} / Tél. portable : {user.cell}
                </Typography>
              </div>
            </CardContent>
          </Card>
        </>
      )}
    </>
  );
};

export default User;
