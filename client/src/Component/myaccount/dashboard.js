import { Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Donut from "react-donut";
import { useQuery } from "react-query";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  align: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    flexWrap: "wrap",
    margin: "O 20% !important",
    justifyContent: "center",
  },
  useralign: {
    display: "flex",
    alignItems: "center",
    flexWrap: "wrap",
    margin: "1% 1% 4% 1% !important",
    justifyContent: "left",
    float: "right",
    padding: "13px 20px",
  },
  username: {
    fontSize: "34px !important",
    marginRight: "10px",
    textAlign: "right"
  },
  totaluser: {
    fontSize: "24px",
    marginRight: "10px",
    textAlign: "right"
  },
  userImg: {
    width: "60px",
    display: "inline-block",
    borderRadius: "100%",
    marginLeft: "20px"
  },
});

export const Dashboard = () => {
  const classes = useStyles();
  let { username } = useParams();

  const [countryStat, setCountryStat] = useState();

  const { data: user } = useQuery("user", () =>
    fetch(`http://localhost:3000/user/${username}`).then((res) => res.json())
  );
  const { data: totalCount } = useQuery("totalCount", () =>
    fetch(`http://localhost:3000/api/getTotalCount`).then((res) => res.json())
  );
  const { data: gender } = useQuery("gender", () =>
    fetch("http://localhost:3000/api/genderStat").then((res) => res.json())
  );
  var arr = [];

  useEffect(async () => {
    fetch("http://localhost:3000/api/countryStat")
      .then((response) => response.json())
      .then((data) => {
        Object.keys(data.data).map((e, i) => {
          arr.push({
            name: Object.keys(data.data)[i],
            data: Object.values(data.data)[i],
          });
        });
        setCountryStat(arr);
      });
    return;
  }, []);
  return (
    <>
      {user && totalCount && gender && countryStat && (
        <div>
          <div className={classes.useralign}>
            <div>
              <Typography className={classes.username}>
                {user.result[0].name.first + ' ' + user.result[0].name.last}
              </Typography>
              <Typography className={classes.totaluser}>{totalCount.data} utilisateurs</Typography>
            </div>
            <img src={user.result[0].picture.medium} className={classes.userImg} />
          </div>
          {
            <>
              <div className={classes.align}>
                  <Donut
                    chartData={countryStat}
                    chartWidth={500}
                    chartHeight={650}
                    title="Statistiques par pays"
                    legendAlignment="top"
                    chartThemeConfig={{
                      series: {
                        colors: [
                          "#0123ff",
                          "#1A39FF",
                          "#334FFF",
                          "#4D65FF",
                          "#667BFF",
                          "#8091FF",
                          "#99A7FF",
                          "#B2BDFF",
                          "#CCD3FF",
                          "#E5E9FF",
                        ],
                      },
                    }}
                  />
                  {gender && (
                    <Donut
                      chartData={[
                        { name: "female", data: gender.female },
                        { name: "male", data: gender.male },
                      ]}
                      chartWidth={500}
                      chartHeight={500}
                      title="Statistique Homme/Femme"
                      legendAlignment="top"
                      chartThemeConfig={{
                        title: {
                          fontFamily: "Righteous !important",
                        },
                        series: {
                          colors: ["#dd3333", "#0123ff"],
                        },
                        chartExportMenu: {
                          backgroundColor: "black",
                        },
                      }}
                    />
                  )}
              </div>
            </>
          }
        </div>
      )}
    </>
  );
};
