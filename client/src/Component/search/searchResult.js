import React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { useNavigate } from "react-router-dom";
import { makeStyles } from "@mui/styles";

import { useQuery } from "react-query";

const useStyles = makeStyles({
  searchCard: {
    borderRadius: "15px !important",
    fontFamily: "Righteous !important",
    background: "red",
    width: "30%",
    margin: "30px 1.49% !important",
    display: "inline-block",
    transition: "0.3s",
    "&:hover": {
      boxShadow: "0 12px 40px -12.125px rgba(0,0,0,0.3)"
    },
  },
  searchCardContent: {
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  img: {
    width: "100%",
    minHeight: "200px",
    marginBottom: "10px",
    borderRadius: "5px !important",
  },
  btn: {
    background: "linear-gradient(123deg, rgba(253,29,29,1) 0%, rgba(252,176,69,1) 100%)",
    color: "#fff !important",
    fontWeight: "bold !important",
    width: "180px",
    marginRight: "0 !important",
    transition: "0.4s ease",
    "&:hover": {
      background: "linear-gradient(123deg, rgba(252,176,69,1) 0%, rgba(253,29,29,1) 100%)",
    },
  },
  resultTitle: {
    fontFamily: "Righteous !important",
    fontSize: "2rem !important",
    textAlign:"left",
  },
  resultName: {
    fontFamily: "Righteous !important",
    fontSize: "1rem !important",
  }
});

export const SearchResult = ({
  gender,
  city,
  state,
  phone,
  firstname,
  lastname,
  query,
}) => {
  const classes = useStyles();
  let navigate = useNavigate();
  const { isLoading, error, data } = useQuery("repoData", () =>
    fetch(
      `api/searchOne/?firstname=${firstname}&lastname=${lastname}&gender=${gender}&state=${state}&city=${city}&phone=${phone}`
    ).then((res) => res.json())
  );
  if (isLoading) return "Loading...";

  if (error) return "An error has occurred: " + error.message;

  const handleClick = (username) => {
    navigate(`/user/${username}`);
  };

  return data.response.map((user) => (
    <><Card sx={{ minWidth: 275 }} className={classes.searchCard}>
      <CardContent className={classes.searchCardContent} onClick={() => handleClick(user.login.username)}>
        <img src={user.picture.large} className={classes.img}></img>
        <div>
          <Typography fontSize={ 26 } onClick={() => handleClick(user.login.username)} className={classes.resultTitle}>
            {user.name.first}
          </Typography>
          <Typography fontSize={ 26 } onClick={() => handleClick(user.login.username)} className={classes.resultTitle}>
            {user.name.last}
          </Typography>
          <Typography className={classes.resultName}>
            {user.email}
          </Typography>
          <Typography className={classes.resultName}>
            {user.phone} / {user.cell}
          </Typography>
        </div>
      </CardContent>
    </Card></>
  ));
};
