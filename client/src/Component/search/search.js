import { TextField, Button } from "@mui/material";
import React, { useState } from "react";
import { SearchResult } from "./searchResult";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  searchInput: {
    width: "60%",
    margin: "40px 20% !important",
    color: "#ececec !important",
  },
  searchBtn: {
    fontSize: "40px !important",
    fontFamily: "Righteous !important",
    padding: "15px !important",
    width: "20%",
    margin: "50px 40% !important",
    borderRadius: "20px",
    display: "block",
    textAlign: "center",
    background: "rgba(0,35,255,1)",
    color: "#fff !important",
    fontWeight: "bold !important",
    transition: "0.4s",
    "&:hover": {
      background: "radial-gradient(circle, rgba(75,100,255,1) 0%, rgba(0,35,255,1) 100%);",
    },
  },
});

const Search = () => {
  const classes = useStyles();

  const [gender, setgender] = useState("");
  const [city, setcity] = useState("");
  const [state, setstate] = useState("");
  const [phone, setphone] = useState("");
  const [firstname, setfirstname] = useState("");
  const [lastname, setlastname] = useState("");
  const [query, setquery] = useState("");

  const HandleSearch = (e) => {
    setquery(true);
  };
  return (
    <>
      <TextField
        className={classes.searchInput}
        label="Prénom"
        id="fullWidth"
        onChange={(e) => setfirstname(e.target.value)}
        value={firstname}
        color="primary"
        variant="standard"
        InputLabelProps={{
          style: {
            color: '#fff'
          }
        }}

      />
      <TextField
        className={classes.searchInput}
        label="Nom"
        id="fullWidth"
        onChange={(e) => setlastname(e.target.value)}
        value={lastname}
        color="primary"
        variant="standard"
        InputLabelProps={{
          style: {
            color: '#fff'
          }
        }}
      />
      <TextField
        className={classes.searchInput}
        label="Genre"
        id="fullWidth"
        onChange={(e) => setgender(e.target.value)}
        value={gender}
        color="primary"
        variant="standard"
        InputLabelProps={{
          style: {
            color: '#fff'
          }
        }}
      />
      <TextField
        className={classes.searchInput}
        label="Pays"
        id="fullWidth"
        onChange={(e) => setstate(e.target.value)}
        value={state}
        color="primary"
        variant="standard"
        InputLabelProps={{
          style: {
            color: '#fff'
          }
        }}
      />
      <TextField
        className={classes.searchInput}
        label="Ville"
        id="fullWidth"
        onChange={(e) => setcity(e.target.value)}
        value={city}
        color="primary"
        variant="standard"
        InputLabelProps={{
          style: {
            color: '#fff'
          }
        }}
      />
      <TextField
        className={classes.searchInput}
        label="Téléphone"
        id="fullWidth"
        onChange={(e) => setphone(e.target.value)}
        value={phone}
        color="primary"
        variant="standard"
        InputLabelProps={{
          style: {
            color: '#fff'
          }
        }}
      />
      <Button onClick={HandleSearch} className={classes.searchBtn}>RECHERCHER</Button>

      {query && (
        <SearchResult
          gender={gender}
          city={city}
          state={state}
          phone={phone}
          firstname={firstname}
          lastname={lastname}
          query={query}
        />
      )}
    </>
  );
};

export default Search;
