# PeoplesStats

Ce projet a été réalisé dans le cadre de ma première année de MBA Developpeur Full-Stack

## Consigne

https://docs.google.com/document/d/1fGWK3FZGiezfEHBir6BqrscRdWXh_poPlfifQaLr0I8/edit

## Technologie

- React JS
- Node
- CSS

## Installer le projet

### `git clone git@gitlab.com:MWMatthieuL/peoplesstats.git`

## Lancer l'application

Afin de pouvoir utiliser l'application PeopleStats, il est nécessaire d'entrer les commandes suivantes depuis un terminal :

- A la racine du projet
    ### `npm i` et `npm start`
- Dans le dossier client
  ### `npm i` et `npm start`

## Routes Disponibles

- /login pour se connecter
- /search pour effectuer une recherche
- /user/(username) pour consulter la fiche d'un utilisateur
- /dashboard pour consulter les statistiques de genre et de nationalité
- /(tout autre chose) pour voir la page d'erreur 404
